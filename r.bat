@echo off
call m.bat
if not errorlevel 1 (
    \sdcard\bin\hdfmonkey put \sdcard\cspect-next-2gb.img elite.nex
    \sdcard\bin\hdfmonkey put \sdcard\cspect-next-2gb.img etc/autoexec.bas /nextzxos
    \sdcard\bin\CSpect.exe -r -brk -s14 -w3 -zxnext -nextrom -map=elite.map -mmc=\sdcard\cspect-next-2gb.img
)

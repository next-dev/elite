;;----------------------------------------------------------------------------------------------------------------------
;; 16-bit Fixed point maths
;;----------------------------------------------------------------------------------------------------------------------

; Multiplies 16x16 and produces a 32-bit value
;
; Input:
;       HL = 16-bit (x)
;       DE = 16-bit (y)
;
; Output:
;       DEHL = 32-bit x * y
;       CF = 0
;
; Destroys:
;       AF, BC

mul16_16_32:    ld      b,l     ; x0
                ld      c,e     ; y0
                ld      e,l     ; x0
                ld      l,d     ; y1
                push    hl      ; x1 y1
                ld      l,c     ; y0

                ; BC = x0 y0
                ; DE = y1 x0
                ; HL = x1 y0
                ; Stack: x1 y1

                mul     d,e     ; DE = y1 * x0
                ex      de,hl
                mul     d,e     ; DE = x1 * y0

                xor     a
                add     hl,de   ; Sum cross products p2 p1
                adc     a,a     ; A = CF

                ld      e,c     ; x0
                ld      d,b     ; y0
                mul     d,e     ; y0 * x0

                ld      b,a     ; Carry from cross products
                ld      c,h     ; LSB of MSW from cross products

                ld      a,d
                add     a,l
                ld      h,a
                ld      l,e     ; LSW in HL p1 p0

                pop     de
                mul     d,e     ; x1 * y1

                ex      de,hl
                adc     hl,bc
                ex      de,hl   ; DE = final MSW

                ret

; Signed multiplies 16x16 and produces a 32-bit value
;
; Input:
;       HL = signed 16-bit (x)
;       DE = signed 16-bit (y)
;
; Output:
;       DEHL = signed 32-bit x * y
;       CF = 0
;
; Destroys:
;       AF, BC

smul_16_16_32:  ld      b,d
                ld      c,h
                push    bc

                bit     7,d
                call    nz,negDE

                bit     7,h
                call    nz,negHL

                call    mul16_16_32

                pop     bc
                ld      a,b
                xor     c
                jp      m,negDEHL

                ret

;;----------------------------------------------------------------------------------------------------------------------
;; Negation routines
;; All destroy AF

negDE:          ld      a,e
                cpl
                ld      e,a
                ld      a,d
                cpl
                ld      d,a
                inc     de
                ret

negHL:          ld      a,l
                cpl
                ld      l,a
                ld      a,h
                cpl
                ld      h,a
                inc     hl
                ret

negDEHL:        ld      a,l
                cpl
                ld      l,a
                ld      a,h
                cpl
                ld      h,a
                ld      a,e
                cpl
                ld      e,a
                ld      a,d
                cpl
                ld      d,a
                inc     l
                ret     nz
                inc     h
                ret     nz
                inc     de
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; Fixed point multiply

; Input:
;       HL = a
;       DE = b
;
; Output:
;       HL = a * b
;
; Destroys:
;       AF, BC, DE

fpMul:          call    smul_16_16_32

                ; DEHL -> 0DEH
                ld      l,h
                ld      h,e
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; Fix point division

; Input:
;       HL = a
;       DE = b
;
; Output:
;       HL = a / b
;
; Destroys:
;       AF, BC, DE

fpDiv:          
                ld      a,d
                and     $f0
                swapnib
                add     32              ; A = page #
                nextreg $57,a

                ex      de,hl
                ld      a,h
                and     $0f
                ld      h,a
                add     hl,hl
                ld      bc,$e000
                add     hl,bc           ; HL = address of value
                ld      a,(hl)
                inc     hl
                ld      h,(hl)
                ld      l,a             ; HL = 1/b
                nextreg $57,1           ; Restore page

                call    fpMul
                ret


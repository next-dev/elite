;;----------------------------------------------------------------------------------------------------------------------
;; Random number generator
;;----------------------------------------------------------------------------------------------------------------------

; Ouput:
;       HL = random number

rand:
                ld      hl,$a280
                ld      de,$c0de
                ld      (rand+4),hl
                ld      a,l
                add     a,a
                add     a,a
                add     a,a
                xor     l
                ld      l,a
                ld      a,d
                add     a,a
                xor     d
                ld      h,a
                rra
                xor     h
                xor     l
                ld      h,e
                ld      l,a
                ld      (rand+1),hl
                ret


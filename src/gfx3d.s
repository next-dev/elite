;;----------------------------------------------------------------------------------------------------------------------
;; 3d graphics routines
;;----------------------------------------------------------------------------------------------------------------------

initL2:
                ; Set up L2 palette
                xor     a
                nextreg NR_EULA_CTRL,%00010000          ; Set L2/0 palette for editing
                nextreg NR_PALETTE_IDX,a

.l1             nextreg NR_PALETTE_VAL,a
                inc     a
                jr      nz,.l1

                ; Set up L2 in the low 48K
                call    present

                ; Enable the L2
                ld      bc,IO_LAYER2
                ld      a,2
                out     (c),a

                call    cls
                call    present

                ret

;;----------------------------------------------------------------------------------------------------------------------
;; Present
;; Show the back buffer and swaps front/back buffer

BackBuffer      db      8

present:
                ld      a,(BackBuffer)
                nextreg NR_L2_PAGE,a            ; Set front buffer
                xor     3                       ; Switch to other buffer 8 <-> 11
                ld      (BackBuffer),a
                sla     a

                nextreg NR_MMU0,a
                inc     a
                nextreg NR_MMU1,a
                inc     a
                nextreg NR_MMU2,a
                inc     a
                nextreg NR_MMU3,a
                inc     a
                nextreg NR_MMU4,a
                inc     a
                nextreg NR_MMU5,a
                inc     a

                ret

;;----------------------------------------------------------------------------------------------------------------------
;; Cls
;; Clears the L2 screen using the DMA

cls:
                xor     a
                ld      h,a
                ld      l,a
                ld      c,a
                ld      b,$c0
                jp      memfill


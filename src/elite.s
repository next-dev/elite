;;----------------------------------------------------------------------------------------------------------------------
;; Elite 3d demo
;;----------------------------------------------------------------------------------------------------------------------

                DEVICE ZXSPECTRUMNEXT
                CSPECTMAP "elite.map"

NUM_STARS       equ     16

;;----------------------------------------------------------------------------------------------------------------------
;; Source code include

                INCLUDE "src/z80n.s"
                INCLUDE "src/keyboard.s"
                INCLUDE "src/memory.s"
                INCLUDE "src/gfx3d.s"
                INCLUDE "src/random.s"
                INCLUDE "src/fp16.s"
                INCLUDE "src/update.s"

;;----------------------------------------------------------------------------------------------------------------------

Start:
                xor     a
                out     ($fe),a
                nextreg NR_CLOCK_SPEED,2        ; 14Mhz!
                call    InitKeys
                call    initL2
                call    cls
                call    present

.loop           
                halt
                call    present
                ld      a,2
                out     ($fe),a
                call    update
                ld      a,6
                out     ($fe),a
                call    cls
                xor     a
                out     ($fe),a
                jr      .loop

;;----------------------------------------------------------------------------------------------------------------------

                display "End of source code: ", $

;;----------------------------------------------------------------------------------------------------------------------
;; Division tables

                MMU     7,32
                org     $e000
                incbin  "src/recip.bin",0,8192
                MMU     7,33
                org     $e000
                incbin  "src/recip.bin",8192,8192
                MMU     7,34
                org     $e000
                incbin  "src/recip.bin",16384,8192
                MMU     7,35
                org     $e000
                incbin  "src/recip.bin",24577,8192
                MMU     7,36
                org     $e000
                incbin  "src/recip.bin",32768,8192
                MMU     7,37
                org     $e000
                incbin  "src/recip.bin",40960,8192
                MMU     7,38
                org     $e000
                incbin  "src/recip.bin",49152,8192
                MMU     7,39
                org     $e000
                incbin  "src/recip.bin",57344,8192
                MMU     7,40
                org     $e000
                incbin  "src/recip.bin",65537,8192
                MMU     7,41
                org     $e000
                incbin  "src/recip.bin",73728,8192
                MMU     7,42
                org     $e000
                incbin  "src/recip.bin",81920,8192
                MMU     7,43
                org     $e000
                incbin  "src/recip.bin",90112,8192
                MMU     7,44
                org     $e000
                incbin  "src/recip.bin",98304,8192
                MMU     7,45
                org     $e000
                incbin  "src/recip.bin",106497,8192
                MMU     7,46
                org     $e000
                incbin  "src/recip.bin",114688,8192
                MMU     7,47
                org     $e000
                incbin  "src/recip.bin",122880,8192

;;----------------------------------------------------------------------------------------------------------------------
;; Nex file generation

                SAVENEX OPEN "elite.nex", Start, $0000
                SAVENEX CORE 2, 0, 0
                SAVENEX CFG 0, 0, 0, 0
                SAVENEX AUTO

;;----------------------------------------------------------------------------------------------------------------------
;;----------------------------------------------------------------------------------------------------------------------
